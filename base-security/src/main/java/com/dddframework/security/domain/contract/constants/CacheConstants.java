package com.dddframework.security.domain.contract.constants;

public interface CacheConstants {
    /**
     * 用户信息缓存
     */
    String USER_CACHE = "user_cache";
    /**
     * 默认验证码前缀
     */
    String VER_CODE_DEFAULT = "ver_code_default:";
}
