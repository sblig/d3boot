package com.dddframework.security.infras.exception;

import com.dddframework.security.infras.exception.BaseAuth2Exception;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.SneakyThrows;

/**
 * OAuth2 异常格式化处理
 *
 * @author zhouzx
 */
public class BaseAuth2ExceptionSerializer extends StdSerializer<BaseAuth2Exception> {

    public BaseAuth2ExceptionSerializer() {
        super(BaseAuth2Exception.class);
    }

    @Override
    @SneakyThrows
    public void serialize(BaseAuth2Exception value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject();
        gen.writeObjectField("code", 1);
        gen.writeStringField("msg", value.getMessage());
        gen.writeStringField("data", value.getErrorCode());
        gen.writeEndObject();
    }
}