package com.dddframework.security.domain.thirdparty.service;

import cn.hutool.http.HttpUtil;
import com.dddframework.core.contract.exception.ServiceException;
import com.dddframework.core.utils.JsonKit;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 认证授权相关工具类
 *
 * @author zhouzx
 */
@Slf4j
@UtilityClass
public class ThirdPartyService {
    String WX_AUTHORIZATION_CODE_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
    String QQ_AUTHORIZATION_CODE_URL = "https://graph.qq.com/oauth2.0/token?client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code&redirect_uri=%s";
    String QQ_AUTHORIZATION_ME_URL = "https://graph.qq.com/oauth2.0/me?access_token=%s";

    /**
     * 微信获取OPENID
     */
    public String getWxOpenId(String appId, String appSecret, String code) {
        String url = String.format(WX_AUTHORIZATION_CODE_URL, appId, appSecret, code);
        String result = HttpUtil.get(url);
        log.debug("微信响应报文:{}", result);
        Map<String, Object> rs = JsonKit.toMap(result);
        if (rs.get("errcode") != null) {
            throw new ServiceException((String) rs.get("errmsg"));
        }
        return (String) rs.get("openid");
    }

    /**
     * QQ获取OPENID
     * https://wiki.connect.qq.com/%E8%8E%B7%E5%8F%96%E7%94%A8%E6%88%B7openid_oauth2-0
     */
    public String getQqOpenId(Integer appId, String appSecret, String code, String redirectUri) {
        String url = String.format(QQ_AUTHORIZATION_CODE_URL, appId, appSecret, code, redirectUri);
        String result = HttpUtil.get(url);
        log.debug("QQ响应报文:{}", result);
        String accessToken = result.split("&")[0].split("=")[1];
        url = String.format(QQ_AUTHORIZATION_ME_URL, accessToken);
        result = HttpUtil.get(url);
        result = result.replace("callback( ", "");
        result = result.replace("); ", "");
        Map<String, Object> map = JsonKit.toMap(result);
        return map.get("openid").toString();
    }
}