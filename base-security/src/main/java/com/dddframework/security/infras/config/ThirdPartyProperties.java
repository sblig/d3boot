package com.dddframework.security.infras.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 第三方登录配置
 *
 * @author zhouzx
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "thirdparty")
public class ThirdPartyProperties {
    // WX配置
    private WXConfig wx = new WXConfig();
    // QQ配置
    private QQConfig qq = new QQConfig();

    @Data
    public static class WXConfig {
        private String appId;
        private String appSecret;
    }

    @Data
    public static class QQConfig {
        private Integer appId;
        private String appKey;
    }
}