package com.dddframework.security.domain.thirdparty.component;

import com.dddframework.core.context.SpringContext;
import com.dddframework.kit.lang.StrKit;
import com.dddframework.security.domain.auth.model.AuthUserChecker;
import com.dddframework.security.domain.thirdparty.model.ThirdPartyAuthToken;
import com.dddframework.security.domain.thirdparty.service.ThirdPartyService;
import com.dddframework.security.domain.auth.service.IUserDetailService;
import com.dddframework.security.infras.config.ThirdPartyProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

/**
 * 第三方登录逻辑
 *
 * @author zhouzx
 */
@Slf4j
@Getter
@Setter
public class ThirdPartyAuthenticationProvider implements AuthenticationProvider {
    private MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();
    private UserDetailsChecker detailsChecker = new AuthUserChecker();
    private IUserDetailService userService;
    private ThirdPartyProperties thirdPartyProperties;

    @Override
    @SneakyThrows
    public Authentication authenticate(Authentication authentication) {
        ThirdPartyAuthToken thirdPartyAuthToken = (ThirdPartyAuthToken) authentication;
        String principal = thirdPartyAuthToken.getPrincipal().toString();
        String[] inStrs = principal.split("@");
        String type = inStrs[0];
        String code = inStrs[1];
        String redirectUri = inStrs[2];
        String openId = null;
        if (StrKit.equals(type, "WX")) {//微信登录
            openId = ThirdPartyService.getWxOpenId(thirdPartyProperties.getWx().getAppId(), thirdPartyProperties.getWx().getAppSecret(), code);
        }
        if (StrKit.equals(type, "QQ")) {
            openId = ThirdPartyService.getQqOpenId(thirdPartyProperties.getQq().getAppId(), thirdPartyProperties.getQq().getAppKey(), code, redirectUri);
        }
        UserDetails userDetails = SpringContext.getBean(IUserDetailService.class).loadByOpenId(type, openId);
        if (userDetails == null) {
            log.debug("Authentication failed: no credentials provided");
            throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.noopBindAccount", "Noop Bind Account"));
        }
        // 检查账号状态
        detailsChecker.check(userDetails);
        ThirdPartyAuthToken authenticationToken = new ThirdPartyAuthToken(userDetails, userDetails.getAuthorities());
        authenticationToken.setDetails(thirdPartyAuthToken.getDetails());
        return authenticationToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return ThirdPartyAuthToken.class.isAssignableFrom(authentication);
    }
}