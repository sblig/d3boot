package com.dddframework.security.domain.contract.event;

import com.dddframework.core.contract.DomainEvent;
import com.dddframework.security.domain.auth.model.AuthUser;
import lombok.Getter;

/**
 * 用户登录成功事件
 */
@Getter
public class UserLoginSuccessEvent extends DomainEvent {
    private AuthUser authUser;

    public <T> UserLoginSuccessEvent(T source, AuthUser authUser) {
        super(source);
        this.authUser = authUser;
    }
}
