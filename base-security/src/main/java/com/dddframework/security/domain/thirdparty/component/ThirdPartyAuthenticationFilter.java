package com.dddframework.security.domain.thirdparty.component;

import com.dddframework.security.domain.thirdparty.model.ThirdPartyAuthToken;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 第三方登录验证filter
 *
 * @author zhouzx
 */
public class ThirdPartyAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String SPRING_SECURITY_FORM_THIRDPARTY_KEY = "thirdparty";
    @Getter
    @Setter
    private String thirdpartyParameter = SPRING_SECURITY_FORM_THIRDPARTY_KEY;
    @Getter
    @Setter
    private boolean postOnly = true;
    @Getter
    @Setter
    private AuthenticationEventPublisher eventPublisher;
    @Getter
    @Setter
    private AuthenticationEntryPoint authenticationEntryPoint;

    public ThirdPartyAuthenticationFilter() {
        super(new AntPathRequestMatcher("/thirdparty/token", "POST"));
    }

    @Override
    @SneakyThrows
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (postOnly && !request.getMethod().equals(HttpMethod.POST.name())) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        String thirdparty = obtainThirdParty(request);
        if (thirdparty == null) {
            thirdparty = "";
        }
        thirdparty = thirdparty.trim();
        ThirdPartyAuthToken thirdPartyAuthToken = new ThirdPartyAuthToken(thirdparty);
        setDetails(request, thirdPartyAuthToken);
        Authentication authResult = null;
        try {
            authResult = this.getAuthenticationManager().authenticate(thirdPartyAuthToken);
            logger.debug("Authentication ok: " + authResult);
            SecurityContextHolder.getContext().setAuthentication(authResult);
        } catch (Exception failed) {
            SecurityContextHolder.clearContext();
            logger.debug("Authentication request failed: " + failed);
            eventPublisher.publishAuthenticationFailure(new BadCredentialsException(failed.getMessage(), failed), new PreAuthenticatedAuthenticationToken("access-token", "N/A"));
            try {
                authenticationEntryPoint.commence(request, response, new UsernameNotFoundException(failed.getMessage(), failed));
            } catch (Exception e) {
                logger.error("authenticationEntryPoint handle error:{}", failed);
            }
        }
        return authResult;
    }

    private String obtainThirdParty(HttpServletRequest request) {
        return request.getParameter(thirdpartyParameter);
    }

    private void setDetails(HttpServletRequest request, ThirdPartyAuthToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }
}