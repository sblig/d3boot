package com.dddframework.security.domain.contract.event;

import com.dddframework.core.contract.DomainEvent;

/**
 * 用户登录失败事件
 */
public class UserLoginFailedEvent extends DomainEvent {
    public <T> UserLoginFailedEvent(T source) {
        super(source);
    }
}
