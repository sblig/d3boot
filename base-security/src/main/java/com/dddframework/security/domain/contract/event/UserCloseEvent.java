package com.dddframework.security.domain.contract.event;

import com.dddframework.core.contract.DomainEvent;

/**
 * 用户关闭事件
 */
public class UserCloseEvent extends DomainEvent {
    public <T> UserCloseEvent(T source) {
        super(source);
    }
}
